## [9.3.35](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.34...9.3.35) (2025-03-10)


### Bug Fixes

* **deps:** update registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core:2.17 docker digest to 3d22dcc ([035dbdf](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/035dbdf9d35699d962d8c70c15b5f1a57471b552))

## [9.3.34](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.33...9.3.34) (2025-02-16)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/kubernetes_collection to v9.1.11 ([9267f48](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/9267f4877f92d4a9529c2ef144000d7720cf70da))

## [9.3.33](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.32...9.3.33) (2025-02-16)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/kubernetes_collection to v9.1.10 ([c8c065d](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/c8c065d27d6f2d1815100541cded3ec25f62695c))

## [9.3.32](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.31...9.3.32) (2025-02-15)


### Bug Fixes

* **deps:** update galaxy packages ([11bfbdf](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/11bfbdfc35193d3f8b8573165efd565dba91f965))

## [9.3.31](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.30...9.3.31) (2025-02-15)


### Bug Fixes

* **deps:** pin dependencies ([3d07304](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/3d07304f92d14242565f8b934bd4a2a30720f674))

## [9.3.30](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.29...9.3.30) (2025-01-21)


### Bug Fixes

* **deps:** update galaxy packages ([8c738e1](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/8c738e1e8a128e20f6d413c935128f7d373ca42c))

## [9.3.29](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.28...9.3.29) (2025-01-20)


### Bug Fixes

* **deps:** update dependency openstack.cloud to >=2.4.0,<2.5.0 ([2381e20](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/2381e2052256a53851933e3d69eb28fdab1bcd62))

## [9.3.28](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.27...9.3.28) (2025-01-09)


### Bug Fixes

* **deps:** update galaxy packages ([d9ce3d3](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/d9ce3d33e884048421ade83c03bd031fdad3af4d))

## [9.3.27](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.26...9.3.27) (2024-12-17)


### Bug Fixes

* **deps:** update galaxy packages ([8be7774](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/8be7774c9c6edb3418b0bc059a02c8b389ecb11a))

## [9.3.26](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.25...9.3.26) (2024-12-13)


### Bug Fixes

* **deps:** update galaxy packages ([ffa54a4](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/ffa54a4fd4a88b1d167e1c54e3c35efb207e51e1))

## [9.3.25](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.24...9.3.25) (2024-11-29)


### Bug Fixes

* **deps:** update dependency openstack.cloud to >=2.3.0,<2.4.0 ([e7fe9f5](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/e7fe9f5a5593c7d2bca8d34d2d82e531032b4d52))

## [9.3.24](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.23...9.3.24) (2024-11-27)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.27 ([4fc880b](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/4fc880be87c703dfbb6f091ab9d2470f421b0882))

## [9.3.23](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.22...9.3.23) (2024-11-21)


### Bug Fixes

* also set hardening profile to "cis" on the agent if running kubernetes>=1.29 ([ebeae62](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/ebeae62fe4d79e9c1810234077b66b01c270654a))

## [9.3.22](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.21...9.3.22) (2024-11-21)


### Bug Fixes

* set hardening profile to "cis" if running kubernetes>=1.29 ([b663c61](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/b663c6124ba1fb6e68b9af823d87e18bed1b0644)), closes [/github.com/rancher/rke2/blob/9e79849266360b1aa27fabe0d981bcc6bc069858/pkg/rke2/rke2.go#L274](https://gitlab.com//github.com/rancher/rke2/blob/9e79849266360b1aa27fabe0d981bcc6bc069858/pkg/rke2/rke2.go/issues/L274)

## [9.3.21](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.20...9.3.21) (2024-10-30)


### Bug Fixes

* **deps:** update galaxy packages ([25d8fc4](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/25d8fc45b1348d11d0c6283de07c6e0224a23116))

## [9.3.20](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.19...9.3.20) (2024-09-25)


### Bug Fixes

* **deps:** update galaxy packages ([59bc410](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/59bc4104bc29977b8eb890096ef46f0e99bcff45))

## [9.3.19](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.18...9.3.19) (2024-06-18)


### Bug Fixes

* **deps:** update galaxy packages ([94f7cdc](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/94f7cdce50278cfac45f33a63465933051ee1458))

## [9.3.18](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.17...9.3.18) (2024-06-14)


### Bug Fixes

* **deps:** update dependency kubernetes.core to v5 ([b5fd6bb](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/b5fd6bb2656fc17ab3dfcee38b83c0787ad837ba))
* **deps:** update dependency orange-opensource/lfn/infra/kubernetes_collection to v9.0.31 ([b042498](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/b04249893276c01f383cd683297f9fb91f8bfd86))

## [9.3.17](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.16...9.3.17) (2024-05-30)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/kubernetes_collection to v9.0.29 ([4343071](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/434307167daa881f572566268917abda794e448c))

## [9.3.16](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.15...9.3.16) (2024-05-30)


### Bug Fixes

* **deps:** update registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core docker tag to v2.17 ([b9563f1](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/b9563f1b378b0113fb9d3133979e4e5e43134249))

## [9.3.15](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.14...9.3.15) (2024-05-24)


### Bug Fixes

* **deps:** update dependency community.general to v9 ([3dd0a1c](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/3dd0a1c35aa62efb1e6d1d30c8adf3d08fee3033))
* **deps:** update galaxy packages ([9ff0bb6](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/9ff0bb6255665d4c07bbb9afb94c96b69fed6b2f))

## [9.3.14](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.13...9.3.14) (2024-04-29)


### Bug Fixes

* **deps:** update galaxy packages ([fab249a](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/fab249a6ad3c96be64374e1c8a6fd11cd4276e61))

## [9.3.13](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.12...9.3.13) (2024-03-26)


### Bug Fixes

* **deps:** update galaxy packages ([24741d4](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/24741d4aaec1e07b1c264788c05e7fb10e75f6e4))

## [9.3.12](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.11...9.3.12) (2024-03-14)


### Bug Fixes

* **deps:** update galaxy packages ([74128a1](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/74128a1f84baccbd4fa765d170fdf3edce87e18f))

## [9.3.11](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.10...9.3.11) (2024-1-31)


### Bug Fixes

* **deps:** update galaxy packages ([2b9280e](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/2b9280ec462182d8c3624ea2a157e90cd66c2ffc))

## [9.3.10](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.9...9.3.10) (2024-01-10)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/kubernetes_collection to v9.0.21 ([9523c79](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/9523c799c3c25d22de41cef9667acaf1389d66e1))

## [9.3.9](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.8...9.3.9) (2024-01-09)


### Bug Fixes

* **deps:** update galaxy packages ([33f2a4b](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/33f2a4be1651a99c8facec278a9dfa9ed0b5c70f))

## [9.3.8](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.7...9.3.8) (2023-12-06)


### Bug Fixes

* **deps:** update galaxy packages ([a1931b0](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/a1931b09aff3bfff8b196e0cdf0f391c97d50d5a))

## [9.3.7](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.6...9.3.7) (2023-11-24)


### Bug Fixes

* **deps:** update galaxy dependencies ([21cd9bf](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/21cd9bfa3ad3c7a56608d505706aa23ce567c30d))

## [9.3.6](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.5...9.3.6) (2023-11-16)


### Bug Fixes

* **deps:** update registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core docker tag to v2.16 ([097ef28](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/097ef28f81e63604f0f5ebd3da124c93e093086f))

## [9.3.5](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.4...9.3.5) (2023-11-14)


### Bug Fixes

* **deps:** update galaxy packages ([26eeb9f](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/26eeb9f1a64a7c837825baddea4f919368f2608f))

## [9.3.4](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.3...9.3.4) (2023-10-13)


### Bug Fixes

* **🔧👽️:** make cilium CNI non exclusive ([d82a1b4](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/d82a1b446292bbeb7a04fc5a8cd48bf5b00dbae4))

## [9.3.3](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.2...9.3.3) (2023-09-19)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/kubernetes_collection to v9.0.15 ([86d908f](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/86d908f3186011ad54cb4ae25b80957fa2b2fa67))

## [9.3.2](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.1...9.3.2) (2023-09-13)


### Bug Fixes

* **deps:** update galaxy packages ([488043b](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/488043b614ba4e5e838757405039381d68cb66a8))

## [9.3.1](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.3.0...9.3.1) (2023-08-31)


### Bug Fixes

* **deps:** update galaxy packages ([798f042](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/798f042af39c126cec6a13244bc3dba687dcab10))

# [9.3.0](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.2.5...9.3.0) (2023-07-24)


### Features

* **deps:** update dependency openstack.cloud to v2 ([b80f864](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/b80f8643bcd5bf907991dbe42919dcd97565a24d))

## [9.2.5](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.2.4...9.2.5) (2023-07-21)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/kubernetes_collection to v9.0.12 ([74ee3de](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/74ee3def89f3d0d2b25e013fcf0e38c5ababe75b))

## [9.2.4](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.2.3...9.2.4) (2023-07-18)


### Bug Fixes

* **deps:** update galaxy packages ([256a8f9](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/256a8f9a965a58f5e787db66213616ecc2eda8f4))

## [9.2.3](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.2.2...9.2.3) (2023-05-17)


### Bug Fixes

* **deps:** update galaxy packages ([f00bc4f](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/f00bc4f7626f0ef2100386ade54ec31e6ab4ee16))

## [9.2.2](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.2.1...9.2.2) (2023-05-06)


### Bug Fixes

* **deps:** update galaxy packages ([1fdcacc](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/1fdcacc0c2527be161431c4bec7a686c4ab42493))

## [9.2.1](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.2.0...9.2.1) (2023-03-28)


### Bug Fixes

* **deps:** update galaxy packages ([eabc4af](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/eabc4afe385c6a013525a55af0ba1cd158b964b6))

# [9.2.0](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.1.2...9.2.0) (2023-03-20)


### Features

* Add a flag to disable rke2 images download on Debian-likes ([8cc3bf2](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/8cc3bf28e010b4f03c997b641836b36ddc14f3be))

## [9.1.2](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.1.1...9.1.2) (2023-02-28)


### Bug Fixes

* **deps:** update galaxy packages ([7c35657](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/7c35657225c059448c436a01b6279e1d460760bb))

## [9.1.1](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.1.0...9.1.1) (2023-02-28)


### Bug Fixes

* **🔧:** allow to not destroyed downloaded binaries ([03e537e](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/03e537e2408fb82941a940764690584b3b7fb48b))

# [9.1.0](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.0.6...9.1.0) (2023-02-25)


### Features

* allow to taint some nodes ([5f33039](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/5f33039daf5a39aee8e2d09e90d8c5f9960fe513))

## [9.0.6](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.0.5...9.0.6) (2023-02-23)


### Bug Fixes

* **deps:** update kubernetes python package to 26.1.0 ([5a01cf5](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/5a01cf54056a75a4467b1f3f89634ae92c5971e8))

## [9.0.5](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.0.4...9.0.5) (2023-02-23)


### Bug Fixes

* **deps:** update python-openstackclient package to 6.0.0 ([72f967d](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/72f967d761baf4825d9112022451be8520fa6341))

## [9.0.4](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.0.3...9.0.4) (2023-02-22)


### Bug Fixes

* **deps:** update openstacksdk python package to 0.62.0 ([3bc6b0a](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/3bc6b0aba0456927e185be0f6333b7fc5cf4c012))

## [9.0.3](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/9.0.2...9.0.3) (2023-02-21)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/kubernetes_collection to v9.0.5 ([4271dec](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/4271dec5f40d647292b15cac8ac988c4fce5f269))

## [9.0.1](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/compare/1.5.2...9.0.1) (2023-02-05)


### Bug Fixes

* **deps:** update galaxy packages ([2e8d4d56](https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection/commit/2e8d4d56ca7ce64ae08bcd4cbe9407f75e27b2fc))

  | datasource        | package                                           | from  | to    |
  | ----------------- | ------------------------------------------------- | ----- | ----- |
  | gitlab-releases   | Orange-OpenSource/lfn/infra/kubernetes_collection | 2.1.7 | 9.0.4 |
  | gitlab-releases   | Orange-OpenSource/lfn/infra/infra_collection      | 9.0.2 | 9.0.3 |
  | galaxy-collection | community.general                                 | 6.2.0 | 6.3.0 |
  | galaxy-collection | kubernetes.core                                   | 2.3.2 | 2.4.0 |
