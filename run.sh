#!/bin/bash

# SPDX-license-identifier: Apache-2.0
##############################################################################
# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
##############################################################################

set -o errexit
set -o nounset
set -o pipefail

labels=$*

RUN_SCRIPT=${0}
RUN_ROOT=$(dirname $(readlink -f ${RUN_SCRIPT}))
export RUN_ROOT=$RUN_ROOT
source ${RUN_ROOT}/scripts/rc.sh

# register our handler
trap submit_bug_report ERR


#-------------------------------------------------------------------------------
# If no labels are set with args, run all
#-------------------------------------------------------------------------------
if [[ $labels = "" ]]; then
  labels="prepare configure deploy postconfiguration"
fi

if [[ $labels = *"prepare"* ]]; then
  #-------------------------------------------------------------------------------
  # Prepare servers
  #  - add needed packages and prepare the servers
  #-------------------------------------------------------------------------------
  step_banner "Prepare servers"
  step_line "Fetch galaxy roles"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/prepare.yml  \
    --vault-id ${RUN_ROOT}/.vault

  step_banner "Servers prepared"
fi

if [[ $labels = *"configure"* ]]; then
  #-------------------------------------------------------------------------------
  # Configure rke2 servers
  #  - retrieve rke2
  #  - configure rke2
  #-------------------------------------------------------------------------------
  step_banner "Configure RKE2"
  if [ -f ${RUN_ROOT}/vars/openstack_user_openrc ]; then
    ## ONLY NEEDED TO SAY IT EXISTS
    ## BETTER WAY TO DO IT?
    step_line "using openstack credentials"
    VAULTED=$(cat ${RUN_ROOT}/vars/openstack_user_openrc | grep -c '^$ANSIBLE_VAULT' || true)
    if [[ ${VAULTED} = "1" ]]; then
      step_line "decrypting credentials"
      ansible-vault decrypt --vault-password-file .vault ${RUN_ROOT}/vars/openstack_user_openrc
    fi
    source ${RUN_ROOT}/vars/openstack_user_openrc
  fi
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/configure.yml \
    --vault-id ${RUN_ROOT}/.vault

  step_banner "RKE2 configured"
fi

#-------------------------------------------------------------------------------
# Install Kubernetes
#  - install kubernetes
#  - install helm
#-------------------------------------------------------------------------------
if [[ $labels = *"deploy"* ]]; then
  step_banner "Install Kubernetes"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/deploy.yml  \
    --vault-id ${RUN_ROOT}/.vault
  step_banner "Kubernetes installed"
fi

#-------------------------------------------------------------------------------
# postconfigure Kubernetes
#  - retrieve kubernetes credentials
#-------------------------------------------------------------------------------
if [[ $labels = *"postconfiguration"* ]]; then
  step_banner "Kubernetes post configuration"
  ansible-playbook ${ANSIBLE_VERBOSE} \
    -i ${RUN_ROOT}/inventory/infra \
    ${RUN_ROOT}/playbooks/postconfigure.yml  \
    --vault-id ${RUN_ROOT}/.vault
    step_banner "Kubernetes post configured"
fi
