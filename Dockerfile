FROM registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core:2.17@sha256:3d22dcc7b4b71763419ccd59688956e6c71e29d4382e7d20e63c620f9073e076

ARG CI_COMMIT_SHORT_SHA
ARG BUILD_DATE

LABEL org.opencontainers.image.source="https://gitlab.com/Orange-OpenSource/lfn/infra/rke2_automatic_installation_collection" \
    org.opencontainers.image.ref.name="RKE2 Installation collection by Orange" \
    org.opencontainers.image.authors="Sylvain Desbureaux <sylvain.desbureaux@orange.com> \
    Nicolas Edel <nicolas.edel@orange.com>" \
    org.opencontainers.image.licenses=" Apache-2.0" \
    org.opencontainers.image.revision=$CI_COMMIT_SHORT_SHA

COPY ansible.cfg /etc/ansible/
ENV APP /opt/rke2_automatic_installation/
ENV COLLECTION_PATH ~/.ansible/collections

WORKDIR $APP

COPY . $APP/

RUN mkdir -p "$COLLECTION_PATH" && \
    git config --global --add safe.directory "$APP/.git" &&\
    ansible-galaxy collection install "git+file://$APP/.git" \
      -p "$COLLECTION_PATH" && \
    rm -rf "$APP/.git" && \
    rm /root/.ansible/collections/ansible_collections/community/general/plugins/modules/java_keystore.py
