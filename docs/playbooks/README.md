# Playbooks

The playbooks supplied with this set of ansible roles are used by a _gating_
validation process using a `chained-ci` pipeline, and thus may be considered
as a good working basis using opnfv pdf and idf files.
