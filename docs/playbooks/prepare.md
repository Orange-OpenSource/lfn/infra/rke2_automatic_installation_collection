# Prepare Playbook

## Purpose

Prepare nodes for an rke2 installation.

In addition to the inventory, an opnfv PDF compatible file must be provided
to describe the nodes

## Inventory

The inventory must provide a `k8s-full-cluster` group to which all cluster nodes
must belong

Search for the `pdf.yml` vars file happens at the same level as inventory
directory

```shell
.
├── inventory
│   └── inventory
└── vars
    └── pdf.yml
```

## Parameters

This playbook doesn't use specific parameters but those required by the invoked
role.
Check parameters of the [orange.rke2.prepare](../roles/prepare.md) role for more
informations.
