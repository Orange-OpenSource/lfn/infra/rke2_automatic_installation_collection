# Postconfigure Playbook

## Purpose

Postconfigure the cluster after it has been sucessfully configured and deployed,
to deal with the kube configuration and make the cluster useable by the
external user without requiring custom system rights.

In addition to the inventory, opnfv PDF and IDF compatible files must be
provided to describe the nodes
Search for the `idf.yml` and `pdf.yml` vars files happens at the same level
as inventory directory.

```shell
.
├── inventory
│   └── inventory
└── vars
    └── idf.yml
    └── pdf.yml
```

## Parameters

This playbook doesn't use specific parameters but those required by the invoked roles.

Check parameters of the [orange.rke2.external_requirements](
../roles/external_requirements.md) and [orange.rke2.postconfigure](
../roles/postconfigure.md) roles for more informations.
