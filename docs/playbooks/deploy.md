# Deploy Playbook

## Purpose

Deploy rke2 processes among the k8s-cluster nodes.

In addition to the inventory, an opnfv IDF compatible file must be provided to
describe the nodes

A sucessfull use of this playbook results in a running k8s cluster (but further
use of postconfigure role/playbook should be considered to connect and use it)

## Inventory

The inventory must provide a `k8s-cluster` group to which all cluster nodes must
belong

Search for the `idf.yml` and optionnal `project_infos.yml` vars file happens at
the same level as inventory directory:

```shell
.
├── inventory
│   └── inventory
└── vars
    └── idf.yml
    └── project_infos.yml
```

## Parameters

This playbook doesn't use specific parameters but those required by the invoked
roles.

Check parameters of the [orange.rke2.external_requirements](
../roles/external_requirements.md) and [orange.rke2.deploy](../roles/deploy.md)
roles for more informations.
