# Configure role

## Purpose

Configure the OS (operating system) with rke2 specific settings and install
appropriate packages.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                                        | Purpose                                                    | Default value                                                                                   |
|-------------------------------------------------|------------------------------------------------------------|-------------------------------------------------------------------------------------------------|
| `kubernetes_version`                            | version of kubernetes to be installed                      | '1.20'                                                                                          |
| `kubernetes_main_address`                       | address (ip or fqdn) of the first rke2 server node         | `hostvars[groups[kubernetes_rke2_control_group][0]].ansible_default_ipv4.address`               |
| `kubernetes_network_plugin`                     | CNI to use                                                 | `"{{ kubernetes_rke2_cni \| default('canal', true) }}"`                                         |
| `kubernetes_rke2_disable_nginx`                 | to disable the default installation of nginx ingress       | `false`                                                                                         |
| `kubernetes_oidc_issuer_url`                    | OIDC issuer url                                            | undefined                                                                                       |
| `kubernetes_oidc_client_id`                     | OIDC client_id                                             | undefined                                                                                       |
| `kubernetes_flat_l3_networks`                   | networks needing to configure Openstack ports              | [calico, kube-router]                                                                           |
| `kubernetes_rke2_control_group`                 | inventory group name for rke2 server nodes                 | 'kube-master'                                                                                   |
| `kubernetes_rke2_worker_group`                  | inventory group name for rke2 agent nodes                  | 'kube-node'                                                                                     |
| `kubernetes_rke2_rhel_airgap_repository`        | define rke2 airgap repository                              | undefined                                                                                       |
| `kubernetes_rke2_containerd_mirrors`            | Configure the mirrors for public repositories              | []                                                                                              |
| `kubernetes_rke2_containerd_repository_configs` | Configure access to a repository                           | []                                                                                              |
| `kubernetes_rke2_local_dns`                     | Do we want to configure localDNS on nodes                  | `false`                                                                                         |
| `kubernetes_rke2_modules`                       | modules we want to load on nodes                           | []                                                                                              |
| `kubernetes_rke2_enable_hardening`              | Boolean to enable rke2 hardening or not                    | `"{{ kubernetes_enable_hardening \| default(false, true) }}"`                                   |
| `kubernetes_rke2_rhel_common_repo`              | URL of rke2 RPM common repository for Centos OS            | <https://rpm.rancher.io/rke2/latest/common/centos/8/noarch>                                     |
| `kubernetes_rke2_rhel_bin_repo`                 | URL of rke2 RPM bin repository for Centos OS               | `"https://rpm.rancher.io/rke2/latest/{{ kubernetes_version }}/centos/8/x86_64"`                 |
| `kubernetes_rke2_rhel_gpg_key`                  | GPG public key for RPM repositories for Centos OS          | <https://rpm.rancher.io/public.key>                                                             |
| `kubernetes_rke2_docker_registry`               | URL of docker registry to use                              | `"{{ kubernetes_docker_proxy \| default('', true) }}"`                                          |
| `kubernetes_rke2_etc_path`                      | rke2 config directory on nodes                             | /etc/rancher/rke2                                                                               |
| `kubernetes_rke2_token_path`                    | path of token file to be used for HA                       | `"{{ kubernetes_rke2_etc_path }}/token"`                                                        |
| `kubernetes_rke2_cni`                           | name of the cni to use                                     | undefined                                                                                       |
| `kubernetes_rke2_upstream_dns`                  | list of upstream dns. If kept as empty list, won't be used | `[]`                                                                                            |
| `kubernetes_rke2_cinder_csi_ca_bundle`          | ca bundle content needed by cinder csi to be used          | undefined                                                                                       |
| `kubernetes_public_loadbalancer_name`           | load balancer name found in `project` for public access    | undefined                                                                                       |
| `kubernetes_internal_loadbalancer_name`         | load balancer name found in `project` for internal access  | undefined                                                                                       |
| `kubernetes_rke2_front_loadbalancer`            | "hardcoded" / computed value of front loadbalancer         | undefined or value found via project info using `kubernetes_public_loadbalancer_name`           |
| `kubernetes_rke2_internal_loadbalancer`         | "hardcoded" / computed value of internal loadbalancer      | undefined or value found via project info using `kubernetes_rke2_internal_loadbalancer`         |
| `kubernetes_falco_backend_installation`         | Do we install Falco backend                                | `false`                                                                                         |
| `kubernetes_falco_gpg_url`                      | URL of Falco GPG key                                       | <https://falco.org/repo/falcosecurity-3672BA8F.asc>                                             |
| `kubernetes_falco_apt_repository`               | URL of Falco apt repository                                | <https://download.falco.org/packages/deb>                                                       |
| `kubernetes_falco_yum_repository`               | URL of Falco yum repository                                | <https://download.falco.org/packages/rpm>                                                       |
| `kubernetes_falcosidekick_url`                  | URL of Falco sidekick to send values                       | undefined                                                                                       |
| `kubernetes_falco_service`                      | which service name to use for Falco                        | `falco-modern-bpf`[^1]                                                                          |
| `proxy_env`                                     |                                                            | Dict of (http_proxy, https_proxy, no_proxy) with values of their respective ansible_env[] value |
| `kubernetes_rke2_cluster_cidr`                  | Cluster pods CIDR                                          | undefined (use rke2 defaults)                                                                   |
| `kubernetes_rke2_service_cidr`                  | Cluster services CIDR                                      | undefined (use rke2 defaults)                                                                   |
| `kubernetes_rke2_node_taint_groups`             | list of all groups for tainting nodes                      | undefined                                                                                       |
| `kubernetes_rke2_node_taint_all_hosts`          | groups where all node that needs to be tainted belongs to  | `tainted-node`                                                                                  |
| `kubernetes_rke2_node_taints`                   | dictionary of taints to apply to nodes                     | undefined                                                                                       |
| `kubernetes_rke2_debian_release_binary_remove`  | do we remove donwloaded binaries?                          | `true`                                                                                          |
| `kubernetes_rke2_debian_download_images`        | Download RKE2 images files                                 | `true`                                                                                          |
<!-- markdownlint-enable line-length -->

[^1]: see
[Falco installation guide](https://falco.org/docs/getting-started/installation/)
for knowing allowed service names.

### Gathering nodes fact

When configuring rke2 in HA mode, server (aka controller) nodes beyond the first
one and all agent (aka worker) nodes need an URL to register against. Default
behaviour of the role is to use the gathered default ipv4 address of the first
controller node.
A valid FQDN may be used.

### High availibity and loadbalancers

In high availibility mode, we need to have a load balancer in front of
Kubernetes API and RKE2 supervisor endpoint.

With that, failed first controller wouldn't break the whole Kubernetes.

In order to keep flows well differenciated, we expect that two load balancers
are provisionned:

* public loadblancer with just the Kubernetes API;
* internal loadbalancer with Kubernetes API and Supersivor endpoint (RKE2 finds
  the Kubernetes via its own IP address).

Public loadbalancer needs two IPs:

* public one;
* internal one.

Internal load balancer needs one IP:

* internal one.

Retrieving these two loadbalancers can be done in two ways:

* provide a valid `project_infos.yml` and gives the name of the two
  loadbalancers via `kubernetes_public_loadbalancer_name` and
  `kubernetes_internal_loadbalancer_name`. It will also update
  `kubernetes_main_address` value;
* give the dictionnary `kubernetes_rke2_front_loadbalancer` and set
  `kubernetes_main_address` to the right value.

#### Using project infos

Here's a valid `project_infos.yml`:

```yaml
project:
  loadbalancers:
    my_front_lb:
      internal_ip: 10.10.10.2
      public_ip: 1.2.3.4
    my_internal_lb:
      internal_ip: 10.10.10.3
```

`project_infos.yml` can be created by hand or generated by a previous part of
the installation process (
[OS Infra Manager](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/)
is able to create the needed load balancers and to generate the output in a
valid format for example).

If `kubernetes_public_loadbalancer_name` is set to `my_front_lb` and
`kubernetes_internal_loadbalancer_name` is set to `my_internal_lb`, then:

* `kubernetes_main_address` will be set to `10.10.10.3`;
* values of endpoint in produced `kube-config` will be
  `https://10.10.10.2:6443`;
* the 3 IPs will be put in the subject alternate (san) list of kubernetes
  certificate.

Nothing prevents to refer to the same loadbalancer as long as this loadbalancer
definition has an `internal_ip` and a `public_ip` (which can be the same).

#### Direct use of values

When loadbalancers IPs are well known IP address, it may be easier to override
the values by doing that:

```yaml
kubernetes_main_address: 10.10.10.3
kubernetes_rke2_front_loadbalancer:
  internal_ip: 10.10.10.2
  public_ip: 1.2.3.4
```

As for previous way, all the values can be the same but they have to be set.

### Airgap

Enabling airgap for rke2 depends on whether the
`kubernetes_rke2_rhel_airgap_repository` var is defined or not. When set, its
value must be an URL for accessing the rke2 specific packages.

When airgap for rke2 is enabled, a GPG public key must be provided either as an
URL or as usual plaintext format

A configuration for a target cluster in a full airgap mode must provide:

* containerd mirrors
* an rke2 packages repository
* a GPG public key for the repository

Example:

<!-- markdownlint-disable line-length -->
```yaml
kubernetes_rke2_containerd_mirrors:
  - repository: docker.io
    mirror: &mymirror https://my.repo.com
  - repository: docker.elastic.co
    mirror: *mymirror
  - repository: gcr.io
    mirror: *mymirror
  - repository: ghcr.io
    mirror: *mymirror
  - repository: k8s.gcr.io
    mirror: *mymirror
  - repository: quay.io
    mirror: *mymirror
kubernetes_rke2_rhel_airgap_repository: &rke2_repo https://my.repo.com/repository/yum-rke2
kubernetes_rke2_rhel_public_key: |
        -----BEGIN PGP PUBLIC KEY BLOCK-----
        mQGNBF5oF/oBDADo7r/dYVfDbsWDmVf1Ma/SbvXPIiS0nEZ1xiDuxYyoMJ0VqKM1
        [...]
        =08O8
        -----END PGP PUBLIC KEY BLOCK-----
```
<!-- markdownlint-enable line-length -->

### CNI

Latest versions of rke2 allow the use of CNI (Container Network Interface)
providers, such as `canal`, `flannel`, `calico`, `cilium`.
More informations here:
<https://rancher.com/docs/rancher/v2.x/en/faq/networking/cni-providers/>
When not specified, the default cni is used

### Proxies

HTTP/HTTPS proxies may be used regardless of the airgap mode.
The content of the `proxy_env` is used to provide appropriate environment
variables wherever this is needed.

Example:

```yaml
proxy_env:
  http_proxy: http://10.1.2.3:3128
  https_proxy: http://10.1.2.3:3128
  no_proxy: noproxy.example.com,10.1.0.0/16
```

### DNS

If you want to set your onw list of DNS where Core DNS will forward requests,
configure `kubernetes_rke2_upstream_dns`.

If kept as empty list, will use `/etc/resolv.conf` file of server hosting
coredns.

Example:

```yaml
kubernetes_rke2_upstream_dns:
  - 9.9.9.9
  - [2003::1]:53
```

### Tainting nodes

Sometimes, you want to taint some nodes in order to allow only specific
workloads to run on it.

In order to do that, you need to use groups and what tolerations to set on these
groups.

Let's say you have 8 workers and you want to add a specific taint on 2 of them
and another taint on 2 others.

First, you need to generate a valid inventory.
If you use
[Os Infra Manager Collection](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/),
you'll need to declare the following group configuration:

```yaml
os_infra_nodes_roles:
  compute01: &simpleWorkerGroups
    - kube-node
  compute02: *simpleWorkerGroups
  compute03: *simpleWorkerGroups
  compute04: *simpleWorkerGroups
  compute05:
    - kube-node
    - tainted-node
    - tainted-node-untrusted
  compute06:
    - kube-node
    - tainted-node
    - tainted-node-untrusted
  compute07:
    - kube-node
    - tainted-node
    - tainted-node-untrusted
  compute08:
    - kube-node
    - tainted-node
    - tainted-node-untrusted
  control01: &controlGroups
    - kube-master
    - etcd
    - lb_kubernetes
    - lb_rke2
  control02: *controlGroups
  control03: *controlGroups

os_infra_roles_group:
  k8s-cluster:
    - kube-master
    - kube-node
  k8s-full-cluster:
    - k8s-cluster
    - jumphost
    - etcd
```

Now, you have to define what will be the tainted groups and their taint value:

```yaml
kubernetes_rke2_node_taint_groups:
  - tainted-node-trusted
  - tainted-node-untrusted

kubernetes_rke2_node_taints:
  tainted-node-trusted: workloadType=trusted:NoExecute
  tainted-node-untrusted: workloadType=untrusted:NoSchedule
```

See Kubernetes taints and tolerations
[documentation](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/)
for more information on taints.
