# External requirements role

## Purpose

This role install needed packages/software needed for further rke2
configuration/installation/administration.

Currently it consists in python packages, and has two distinct parts:

- first part is OS dependant: the python pip package using the builtin OS
  packaging system
- second part is OS independant: a set of python pip packages

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                          | Purpose                                           | Default value                   |
|-----------------------------------|---------------------------------------------------|---------------------------------|
| `python_pip_repository`           | repository for "pip"                              | <https://pypi.org/simple/>      |
| `kubernetes_rke2_python_packages` | list of python packages to be installed using pip | * openstacksdk                  |
|                                   |                                                   | * os-client-config==2.0.0       |
|                                   |                                                   | * python-openstackclient==5.1.0 |
|                                   |                                                   | * openshift==0.11.2             |
<!-- markdownlint-enable line-length -->
## Examples

```yaml
- hosts: k8s-cluster
  roles:
    - orange.rke2.external_requirements

- hosts: k8s-cluster
  vars:
    python_pip_repository: https://mirror.example.com/
  roles:
    - orange.rke2.external_requirements
```
