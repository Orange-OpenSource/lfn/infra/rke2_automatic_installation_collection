# Prepare role

## Purpose

Prepare system for rke2 installation, eg configure volumes, etcd, storage, etc.
Cluster nodes settings must be described using the OPNF PDF settings file format.
This role should be used on every node of the k8s cluster.

**Please refer to [`orange.k8s.prepare` role](
https://gitlab.com/Orange-OpenSource/lfn/infra/kubernetes_collection/-/blob/master/docs/roles/prepare.md)
documentation.**
