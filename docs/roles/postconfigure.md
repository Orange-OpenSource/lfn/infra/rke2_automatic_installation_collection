# Postconfigure role

## Purpose

- Handle kube config and make it available to user
- Configure MetalLB kubernetes load-balancer when using baremetal nodes

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                                           | Purpose                                               | Default value                          |
|----------------------------------------------------|-------------------------------------------------------|----------------------------------------|
| `kubernetes_rke2_control_group`                    | inventory group name for rke2 server nodes            | 'kube-master'                          |
| `kubernetes_rke2_worker_group`                     | inventory group name for rke2 agent nodes             | 'kube-node'                            |
| `kubernetes_rke2_etc_path`                         | directory hosting rke2 configuration                  | /etc/rancher/rke2                      |
| `kubernetes_rke2_run_path`                         | rancher rke2 install directory                        | /var/lib/rancher/rke2                  |
| `kubernetes_rke2_bin_path`                         | rancher rke2 binaries directory                       | `"{{ kubernetes_rke2_run_path }}/bin"` |
| `kubernetes_rke2_etc_metallb_dir`                  | directory hosting MetalLB configuration               | /etc/metallb                           |
| `kubernetes_rke2_network_address_pools`            | array of ip address pools to be used by metallb       | [ 10.253.0.226-10.253.0.253 ]          |
| `kubernetes_charts_cinder_openstack_secret`        | name of the secret for cinder csi                     | `cloud-config`                         |
| `kubernetes_charts_cinder_namespace`               | namespace where cinder csi will be deployed           | `cinder-system`                        |
| `kubernetes_rke2_cinder_csi_ca_bundle`             | ca bundle content needed by cinder csi to be used     | undefined                              |
| `kubernetes_charts_openstack_cloud_lb_subnet_name` | subnet name for loadbalancers                         | `admin-subnet`                         |
| `kubernetes_charts_openstack_cloud_secret`         | name of the secret for openstack cloud controller     | `openstack-ingress`                    |
| `kubernetes_charts_openstack_cloud_config_map`     | name of the config map for openstack cloud controller | `openstack-ingress`                    |
| `kubernetes_rke2_automated_upgrade_label`          | name of the label that will be put to trigger upgrade | undefined                              |
<!-- markdownlint-enable line-length -->

## Load balancers

### Metal LB

MetalLB load-balancer are set up when the cluser nodes are bare metal nodes.

Bare metal nodes must have a node.type=baremetal attribute, as set in the OPNFV
PDF settings file.

### Octavia

Octavia LB support is not installed here but preparation work is done.
In order to work, a cloud config file must be provided in vars (name:
`user_clouds.yml`) with the following content:

```yaml
clouds:
  cloud_name:
    auth:
      auth_url: https://my-openstack:5000
      password: REDACTED
      project_name: my-project
      user_domain_name: Default
      username: my-user
    identity_api_version: '3'
    interface: public
    project_domain_name: Default
    region_name: RegionOne
```

Please note that first entry will be taken if several provided.

A project info file has also to be provided in vars (name: `project_infos.yml`
) with the following content:

```yaml
project:
  subnets:
    - name: subnet1
      id: 1234
    - name: subnet2
      id: 1235
    - name: subnet3
      id: 1236
  public_network_id: 6789
```

`kubernetes_charts_openstack_cloud_lb_subnet_name` must be in the subnets list.

## CSI

### Cinder

Cinder support is not installed here but preparation work is done.
In order to work, a cloud config file must be provided in vars (name:
`user_clouds.yml`) with the following content:

```yaml
clouds:
  cloud_name:
    auth:
      auth_url: https://my-openstack:5000
      password: REDACTED
      project_name: my-project
      user_domain_name: Default
      username: my-user
    identity_api_version: '3'
    interface: public
    project_domain_name: Default
    region_name: RegionOne
```
