# Deploy role

## Purpose

Start all previously prepared and configured rke2 nodes

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                        | Purpose                                            | Default value                                                                          |
|---------------------------------|----------------------------------------------------|----------------------------------------------------------------------------------------|
| `kubernetes_main_address`       | address (ip or fqdn) of the first rke2 server node | `"{{ hostvars[groups[kubernetes_rke2_control_group][0]].ansible_default_ipv4.address"` |
| `kubernetes_rke2_control_group` | inventory group name for rke2 server nodes         | 'kube-master'                                                                          |
| `kubernetes_rke2_worker_group`  | inventory group name for rke2 agent nodes          | 'kube-node'                                                                            |
<!-- markdownlint-enable line-length -->

### Gathering nodes fact

When deploying rke2 in HA mode, server (aka controller) nodes beyond the first
one and all agent (aka worker) nodes need an URL to register against.
Default behaviour of the role is to use the gathered default ipv4 address of the
first controller node. A valid FQDN may be used.
