# RKE2 Automatic installation collection

Install RKE2 via an ansible collection

## Goal

This project aims to automatically install a Kubernetes pod using IDF/PDF
description files from OPNFV and few variables.

## Automatic deployment

Today, this code is automatically deployed on Orange POD. You can look at
`.gitlab-ci.yml` to see how it's done.

## Usage

The roles/playbooks assume good PDF/IDF files are provided.
See OPNFV PDF/IDF description file to know how to map with your needs.

In addition to these files you will also need to provide an ansible inventory
to describe the k8s cluster, especially which nodes should be used as simple
k8s worker nodes and which ones should be used as controller nodes

Example:

```ini
[k8s-cluster]
master1.example.com
worker1.example.com
worker2.example.com

[kube-master]
master1.example.com

[kube-node]
worker1.example.com
worker2.example.com

[monitoring]
monitoring.example.com

[k8s-full-cluster:children]
k8s-cluster
monitoring
```

### VM or Baremetal

Almost all tasks of this collection are agnostic on the nodes type.
Only the postconfigure role may configure a specific kind of load-balancer when
using baremetal.
Refer the the postconfigure role
[documentation](./docs/playbooks/postconfigure.md) for more informations.

### Hardening

When configuring rke2, you may use either the `kubernetes_rke2_enable_hardening`
or the `kubernetes_enable_hardening` option to enable the builtin rke2
hardening.

Refer to [RKE2 Hardening guide](https://docs.rke2.io/security/hardening_guide)
for more info.

### Airgap and proxies

This collection supports full airgap installation mode as well as the use of
http(s) proxies.
This means you may specify your own registries/repositories for:

* OS packages
* Python PIP packages
* RKE2 packages

Refer to the `orange.rke2.configure` role
[documentation](./docs/roles/configure.md) for vars listing and
examples.

## Output

* artifacts:
  *vars/kube-config

## Monitoring

If monitoring is enabled, we first need a group `monitoring` with at least one
server. We'll install prometheus and grafana on this server and node_exporter to
all servers.
